[The overview of steps to make MX Respin Budgie from mx-23_CLI_x64.iso] 

reference:
https://wiki.mate-desktop.org/introduction/installation/#manual-installation

https://mxlinux.org/wiki/xfce/how-to-completely-remove-xfce/

--------------------------------------
[Procedure]

apt update

//dpkg -l | grep .xfce. | awk '{print $2}' | xargs apt remove -V --auto-remove -yy
//[Revised and optimized for xfce-minimal]
//apt purge xfce4 exo-utils libexo-2-0 libexo-common libexo-helpers libgarcon-1-0 libgarcon-common libnotify-bin libthunarx-3-0 libtumbler-1-0 libwnck-common libwnck22 libxfce4panel-2.0-4 libxfce4ui-1-0 libxfce4ui-2-0 libxfce4ui-common libxfce4ui-utils libxfce4util-bin libxfce4util-common libxfce4util7 light-locker pavucontrol tango-icon-theme thunar thunar-data thunar-volman tumbler tumbler-common xfce4-appfinder xfce4-notifyd xfce4-panel xfce4-pulseaudio-plugin xfce4-session xfce4-settings xfconf xfdesktop4 xfdesktop4-data xfwm4 thunar-archive-plugin thunar-media-tags-plugin tumbler-plugins-extra xfce4-goodies xfce4-power-manager

apt remove dkms grub-themes-mx

//apt remove xfce* exo-utils desktop-defaults-mx-xfce* libexo* libgarcon* libxfce* thunar* xfwm4 

//apt remove xfdesktop4-data

apt upgrade

apt install xorg xserver-xorg-video-all xserver-xorg-input-all

apt install dbus-x11

apt install budgie-desktop nemo

//desktopfolder --- this is optional, but buggy (it might have much better functions like cut and past on Desktop than budgie-desktop-view.)

exit

nano

# Make ~./xinitrc file using nano and put the line below:
# exec budgie-desktop
# 
# Reference:
# https://wiki.archlinux.jp/index.php/Budgie#.E8.B5.B7.E5.8B.95
# https://wiki.archlinux.jp/index.php/Trinity#startx_.E3.81.A7.E8.B5.B7.E5.8B.95.E3.81.99.E3.82.8B.E3.82.88.E3.81.86.E3.81.AB.E8.A8.AD.E5.AE.9A
# https://wiki.archlinux.jp/index.php/Xinit#xinitrc

# Exit to demo user to enable graphical login.

startx

# Login to complete installation

apt remove dpkg-dev g++ gcc libc6-dev make fakeroot open-vm-tools

apt install budgie-desktop-doc budgie-desktop-view gnome-tweaks nemo-fileroller

apt install avahi-daemon libnss-mdns anacron alsa-utils

# Install below to use wireless network.

apt install anacron avahi-autoipd bluetooth powertop iw wireless-tools wpasupplicant

# Network-manager-gnome will be installed automatically.
//network-manager-pptp-gnome network-manager-gnome

# The below is already installed.
//apt install libdbus-glib-1-2

apt install lightdm

apt install desktop-defaults-mx-common mx-tweak

apt install mx-docs mx-snapshot mx-live-usb-maker mx-packageinstaller formatusb synaptic

apt install eog evince gnome-terminal

apt install cups cups-pdf

apt install system-config-printer

apt install audacious gnome-sound-recorder vlc vlc-l10n

apt install quick-system-info-gui

# Fonts below are optional to add.
# apt install fonts-droid-fallback fonts-ipafont-mincho

apt install uim-anthy uim im-config zenity kasumi uim-gtk2.0 uim-gtk3 uim-qt5 uim-xim uim-fep fonts-vlgothic

apt install uim-chewing uim-pinyin fonts-wqy-microhei

apt install sylpheed sylpheed-i18n aspell-en

apt install firefox-esr firefox-esr-l10n-de firefox-esr-l10n-es-es firefox-esr-l10n-el firefox-esr-l10n-fr firefox-esr-l10n-hi-in firefox-esr-l10n-hr firefox-esr-l10n-hu firefox-esr-l10n-id firefox-esr-l10n-it firefox-esr-l10n-ja firefox-esr-l10n-nl firefox-esr-l10n-pl firefox-esr-l10n-pt-br firefox-esr-l10n-ru firefox-esr-l10n-sv-se firefox-esr-l10n-zh-tw

//apt install firefox-l10n-xpi-de firefox-l10n-xpi-es-es firefox-l10n-xpi-fr firefox-l10n-xpi-hi-in firefox-l10n-xpi-hr firefox-l10n-xpi-hu firefox-l10n-xpi-id firefox-l10n-xpi-it firefox-l10n-xpi-ja firefox-l10n-xpi-nl firefox-l10n-xpi-pt-br firefox-l10n-xpi-ru firefox-l10n-xpi-sv-se firefox-l10n-xpi-zh-tw

//apt install libreoffice-l10n-de libreoffice-l10n-es libreoffice-l10n-fr libreoffice-l10n-hi libreoffice-l10n-hr libreoffice-l10n-hu libreoffice-l10n-id libreoffice-l10n-it libreoffice-l10n-ja libreoffice-l10n-nl libreoffice-l10n-pt-br libreoffice-l10n-ru libreoffice-l10n-sv libreoffice-l10n-zh-cn libreoffice-l10n-zh-tw

# No need to do the below.
//system-locales-mx
Add:
de_DE.UTF-8... done - German locale for Germany
en_US.UTF-8... done - English locale for the USA
es_ES.UTF-8... done - Spanish locale for Spain
fr_FR.UTF-8... done - French locale for France
hi_IN.UTF-8... done - Hindi language locale for India
hr_HR.UTF-8... done - Croatian locale for Croatia
hu_HU.UTF-8... done - Hungarian locale for Hungary
id_ID.UTF-8... done - Indonesian locale for Indonesia
it_IT.UTF-8... done - Italian locale for Italy
ja_JP.UTF-8... done - Japanese language locale for Japan
nl_NL.UTF-8... done - Dutch locale for the Netherlands
pt_BR.UTF-8... done - Portuguese locale for Brasil
ru_RU.UTF-8... done - Russian locale for Russia
sv_SE.UTF-8... done - Swedish locale for Sweden
zh_CN.UTF-8... done - Chinese locale for Peoples Republic of China
zh_TW.UTF-8... done - Chinese locale for Taiwan R.O.C.

apt install geany geany-plugin-spellcheck

apt install libgdk-pixbuf2.0-bin

# To enable sound, install pulseaudio replacing with pipe-wire as below.
# Pipe-wire is so buggy to output sound.

apt install pulseaudio

apt autoremove

apt clean

END.
